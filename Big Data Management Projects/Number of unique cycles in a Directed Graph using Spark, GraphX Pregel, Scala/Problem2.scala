package comp9313.proj2
/*
 * Manish Manandhar z5174497
 * comp9313 Project 2, Problem 2
 * This scala program reads a textfile,
 * 	...creates a graph
 * 	... then uses pregel to find cycles of length k each vertex conatins
 * 	... then filters out the duplicate cylcles to output unique cycles in the graph
 * */

import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object Problem2 {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("proj2_graphX").setMaster("local")
    val sc = new SparkContext(conf)

    val fileName = args(0)
    val k = args(1).toInt
    //val k: Int = 2 //for debugging purposes
    
    //not important to find cycles, as in our problem, weights for edges are not considered
    val weight_placeholder: Double = 0.0
    val edges = sc.textFile(fileName)
    val edgelist = edges.map(x => x.split(" ")).map(x => Edge(x(1).toLong, x(2).toLong, 0.0))
    //create a graph using Spark graphX
    val graph = Graph.fromEdges[Double, Double](edgelist, 0.0)

    //initialize empty SET of Lists of VertexID's to each vertex
    val initialGraph = graph.mapVertices((id, _) => Set[List[VertexId]]())

    val run_pregel = initialGraph.pregel(Set[List[VertexId]](), k, EdgeDirection.Out)(

        // VPORG: Receive messages sent from superstep, S-1
        //gets rid of previous information on the node, as we only care about k cycles
      (id, nodeSet, newNodeSet) => newNodeSet,

      // Send Message to neighbors
      // when the source Vertex has no message(i.e. empty) to send, send Set[List[VertexId]] 
      // when its not empty, right append source id to each List inside the Set
      // appending SOURCE VERTEX ID will help DESTINATION IDs to differntiate messages coming from various nodes
      triplet => { 
        
        if (triplet.srcAttr.isEmpty) {
            val l = triplet.srcId :: Nil
            val newSet = Set(l)
            //println("TRIPLET:\t" + triplet.srcId + " -->" + triplet.dstId + "\t= " + newSet)
            Iterator((triplet.dstId, newSet))
        } else {
            val newSet = for (listInSet <- triplet.srcAttr) yield {
              val newL = listInSet :+ triplet.srcId
              newL
            }
            //println("TRIPLET:\t" + triplet.srcId + " -->" + triplet.dstId + "\t= " + newSet)
            Iterator((triplet.dstId, newSet))
        }
      },

      //merge multiple messages arriving at same vertex, at start of superstep
      //...before VPROG
      (a, b) => a ++ b 
      )//end of run pregel
       
    val final_data = run_pregel.vertices.collect()
    
    //filters cycles from other non-cycles and cycles of length l only
    val cycles_for_each_vertex = final_data
      .map((x=> x._2.filter(lis=> lis(0)==x._1 && lis.toSet.size==k))
      )
    
    //maps each list to a set, to use it later to remove duplicate cylces
    val setOfSets = cycles_for_each_vertex.map(x=>x.map(y=>y.toSet))
        
    val flattenedArrayofAllCycles = setOfSets.flatten.toArray
    
    val cyclesOfLenK = flattenedArrayofAllCycles.toSet
    
    println("\n======FINAL SET======\n")
    cyclesOfLenK.foreach(println)
    
    println("\n\n========================================\n")
    println("Uniques Cycles of length " + args(1) + " in given graph: " + cyclesOfLenK.size)
    println("\n========================================\n\n")
    
    }//end of main

  }//end of Problem 2