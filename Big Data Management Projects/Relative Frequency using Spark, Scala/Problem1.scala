package comp9313.proj2

/*
 * Author: Manish Manandhar
 * z5174497
 * COMP9313: Big Data
 * Project 2: Problem 1
 * Calculates term frequency of co-occurent terms in a document
 * */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object Problem1 {
  def main(args: Array[String]) {
    val inputFile = args(0)
    val outputFolder = args(1)
    val conf = new SparkConf().setAppName("proj2_Problem1").setMaster("local")
    val sc = new SparkContext(conf)

    //read from input textfile
    val textFile = sc.textFile(inputFile)

    //get pair of each word with a following word along, and map it to one
    //Format:[(wi,(wj,1)),...]
    val word_wordJ_count_pair_raw = textFile.flatMap(lines => {
      val line = lines.toLowerCase().split("[\\s*$&#/\"'\\,.:;?!\\[\\](){}<>~\\-_]+")
      //nested for loop that goes through each word, and its succeeding words
      for {
        i <- 0 until line.length
        start = i + 1
        end = line.length - 1
        j <- start to end
      } yield (line(i), (line(j), 1))
    })

    // filter to lowercase and check length, and only pass terms starting from a to z as specified in specs
    val word_wordJ_count_pair = word_wordJ_count_pair_raw.map(x => (x._1.toLowerCase(), (x._2._1.toLowerCase(), x._2._2)))
      .filter(x => (x._1.length >= 1 && x._2._1.length >= 1))
      .filter(x => x._1.charAt(0) <= 'z' && x._1.charAt(0) >= 'a')
      .filter(x => x._2._1.charAt(0) <= 'z' && x._2._1.charAt(0) >= 'a')

    //  (wi,SUM(wi))
    val keyWordAggregated = word_wordJ_count_pair.map(x => (x._1, x._2._2.toDouble))
        .reduceByKey(_ + _)

    // first (wi,(wj,1)) grouped by key, wi,
    //then, group by wj to get sum(wj)
    //then joined with keyWordAggregated (wi,SUM(wi)) to get 
    // to get (wi, (wj,SUM(wj), SUM(wi)) for each wi
    val word1_Word2_Count_TotalSum = word_wordJ_count_pair.groupByKey()
      .flatMapValues(_.groupBy(_._1)
          .mapValues(_.unzip._2.sum.toDouble)) join keyWordAggregated

    //ascending sort by Wj
    val rF_Wj_sorted = word1_Word2_Count_TotalSum.map(x => {
      (x._2._1._1, (x._1, x._2._1._2 / x._2._2.toDouble))
    }).sortByKey()

    //descending sort by frquency
    val rF_freq_sorted = rF_Wj_sorted.map(x => {
      (x._2._2, (x._2._1, x._1))
    }).sortByKey(false)

    //ascending sort by Wi, and mapped to match the specs output format
    //format: Wi Wj Frequency
    val rF_Wi_sorted = rF_freq_sorted.map(x => {
      (x._2._1, (x._2._2, x._1))
    }).sortByKey().map(y => y._1 + " " + y._2._1 + " " + y._2._2)
    
    //rF_Wi_sorted.foreach(println)
    
    //write to text
    rF_Wi_sorted.saveAsTextFile(outputFolder)

  }
}
