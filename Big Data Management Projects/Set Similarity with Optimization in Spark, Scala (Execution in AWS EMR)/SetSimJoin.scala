package comp9313.proj3

/*
 * Finding similar records using a RS Join with a certain Threshold Tau
 * 
 * Author: Manish Manandhar
 * COMP9313: Big Data, Project 3
 * Due: October 28, 2018
 * 
 * */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf


object SetSimJoin {
  
  def main(args: Array[String]) {

    val file1_loc = args(0)
    val file2_loc = args(1)
    val outputLocation = args(2)
    val tau = args(3).toDouble
    //======================================================================
    // // FOR: DEVELOPMENT on a local machine
    
    //val conf = new SparkConf().setAppName("SetSimJoin").setMaster("local")
    
    //======================================================================
    // //FOR: AWS EMR, and SUBMISSION
    
    val conf = new SparkConf().setAppName("SetSimJoin")
    
    //======================================================================
    val sc = new SparkContext(conf)
    val file1 = sc.textFile(file1_loc) 
    val file2 = sc.textFile(file2_loc)
    
    //(elementID:String, Frequency:Int)
    //gets frequency dictionary for each token in FILE 2
    val file2_EleID_Freq_pairs = file2.flatMap(lines => {
      val line = lines.toLowerCase().split(" ")
      for {
        i <- 1 until line.length
      } yield ((line(i), 1))
    }).reduceByKey(_+_).collectAsMap()
   
    // broadcast the frequency dic so all cluster can access it
    val frequency_dic = sc.broadcast(file2_EleID_Freq_pairs)
    
    //reading file 1
    val file1_unsorted = file1.flatMap(lines => {
      val line = lines.toLowerCase().split(" ")
      for {
        i <- 1 until line.length
      } yield (line(0),line(i))
    }).groupByKey().map(x=>(x._1.toInt,x._2.toArray))
    
    //reading file 2
    val file2_unsorted = file2.flatMap(lines => {
      val line = lines.toLowerCase().split(" ")
      for {
        i <- 1 until line.length
      } yield (line(0),line(i))
    }).groupByKey().map(x=>(x._1.toInt,x._2.toArray))

    //===================================================
    //Stage 1: sorting tokens by their frequency in ascending order
    //    ...using frequency dictionary of {elements: freq} of file2
    //===================================================
    val file1_sorted = file1_unsorted.map(x=>(x._1,x._2.
        sortBy(x=>(
            (if (frequency_dic.value.contains(x))
              {(frequency_dic.value(x))}
            else
              {(9999999)}
            ),x))))
        .map(x => (x._1.toInt, x._2.mkString(" "))).sortByKey().map(x=>x._1+" "+x._2)
      
    val file2_sorted = file2_unsorted.map(x=>(x._1,x._2.
        sortBy(x=>(
            (if (frequency_dic.value.contains(x))
              {(frequency_dic.value(x))}
            else
              {(9999999)}
            ),x))))
        .map(x => (x._1.toInt, x._2.mkString(" "))).sortByKey().map(x=>x._1+" "+x._2) 
    
    //===================================================
    // Stage 2: getting (InFrequent Element,(RID, EID* EID* ...))...
    //            and  (InFrequent Element), (S,(SID, EID* EID* ...))
    //
    // Stage 3: removing duplicate records
    //===================================================
    
    //length of R is set as length of line -1, since the record id should not be counted in length
    //output format: (1,Set(0,1 4 5 6)), where 1 is infrequent Element, 0 is the RecordID 1456 is the set of Record ID=0
    val rID_pairs = file1_sorted.map(line => (line.split(" "))).flatMap(
      line =>{
           for( i <- 1 to ((line.length-1 - Math.ceil(tau * (line.length-1))).toInt + 1))
              yield{
              (line(i).toInt, (line(0), line.takeRight(line.length-1).mkString(" ")))
              }
      }) 
      .groupByKey().map(x=>(x._1,x._2.toSet))
      
      //similarly, sID pairs were founded
      val sID_pairs = file2_sorted.map(line => (line.split(" "))).flatMap(
      line =>{
           for( i <- 1 to ((line.length-1 - Math.ceil(tau * (line.length-1))).toInt + 1))
              yield{
              (line(i).toInt, (line(0), line.takeRight(line.length-1).mkString(" ")))
              }
      })
      .groupByKey().map(x=>(x._1,x._2.toSet))

      //inner join, also prunes some lonely RIDs and lonely SID's
      //output format: (CommonInFrequentEement,   (Set(records in R), Set(records in S)   ))
      //        ...(5,   (Set((0,1 4 5 6), (2,4 5 6)),   Set((1,2 5 6), (2,3 5))))
      val joined_rID_sID = rID_pairs.join(sID_pairs)
      
      //now for each infrequent ID, we find the similarities between records of it in R versus S
      val all_prefix_sim = joined_rID_sID
      .flatMap(x=>(
          for (rRecord <- x._2._1 )
            yield{
              for (sRecord <- x._2._2)
                yield{
                val rID = rRecord._1.toInt
                val rSet = rRecord._2.split(" ").toSet
                
                val sID = sRecord._1.toInt
                val sSet = sRecord._2.split(" ").toSet
                
                val numerator = rSet.intersect(sSet)
                val denominator = rSet.union(sSet)
                
                val sim = numerator.size.toDouble/denominator.size.toDouble
                val sim_formatted = BigDecimal(sim).setScale(6,BigDecimal.RoundingMode.HALF_UP).toDouble
                ((rID,sID),sim_formatted)
              }
            }
          )
       ).flatMap(x => x).distinct().filter(x=>x._2>=tau).sortBy(x=>(x._1._1,x._1._2)).map(x=>((x._1._1,x._1._2)+"\t"+x._2))

       val finalResult = all_prefix_sim
//       all_prefix_sim.foreach(println)
       finalResult.saveAsTextFile(outputLocation)

      
    
    
  }
}
