// author: Manish Manandhar
//COMP9313 
//due: 9 Sept, 2018

package comp9313.proj1;

import java.lang.Math;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.net.URI;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.DoubleWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.IntWritable;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;

import comp9313.proj1.StringPair;
import comp9313.proj1.StringIntPair;

public class Project1 {

	// MAPPER to get Term Frequency withing a document (ie Line)
	public static class TFMapper extends Mapper<Object, Text, StringPair, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		// INPUT: TextFile
		// Output KEY: String Pair of 
		// Output VALUE: Int 1 to each (*,DOCID) and (term,DOCID) pairs
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] line = value.toString().split("\n");
			for (String word : line) {
				StringTokenizer itr = new StringTokenizer(word);
				String docID = itr.nextToken().toString();
				StringPair countDocPair = new StringPair();
				countDocPair.set("*", "*");
				context.write(countDocPair, one);

				while (itr.hasMoreTokens()) {
					StringPair idWordPair = new StringPair();
					String term = itr.nextToken().toLowerCase();

					idWordPair.set(term, docID);
					context.write(idWordPair, one);

					idWordPair.set("*", docID);
					context.write(idWordPair, one);
				}
			}

		}
	}

	//TF COMBINER
	//INPUT key: StringPair, value: Intwritable
	//OUTPUT KEY: String Pair and Intwritable
	// combines same terms of a particular mapper
	public static class TFCombiner extends Reducer<StringPair, IntWritable, StringPair, IntWritable> {

		private IntWritable result = new IntWritable();

		public void reduce(StringPair key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}

			result.set(sum);
			context.write(key, result);
		}
	}

	//Not used since this task only has 1 reducer
	public static class TFPartitioner extends Partitioner<StringPair, IntWritable> {

		public int getPartition(StringPair key, IntWritable value, int numPartitions) {
			return (key.getFirst().hashCode() & Integer.MAX_VALUE) % numPartitions;
		}

	}

	//TF Reducer
	// (*,*) for each line also counts the totla number of lines, alternative way to calculate number of documents
	// this also reduces the term frequency of each term in the document it exits upon
	// also sorted by only TERMS at this point
	//writes to a temp folder in format (TERM DOCID TF TOTAL_DOCUMENTS_IN_VOCABULARY)
	public static class TFReducer extends Reducer<StringPair, IntWritable, StringIntPair, StringPair> {
		private DoubleWritable result = new DoubleWritable();
		private double curMarginal = 0;
		private double totalDOCs = 0.0;

		public void reduce(StringPair key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;

			for (IntWritable val : values) {
				sum += val.get();
			}

			String second = key.getFirst();
			String first = key.getSecond();

			//this counts number of lines
			//alternative way to count lines, DIDNT USE IN THIS PROGRAM HOWEVER
			if (first.equals("*") & second.equals("*")) {
				totalDOCs = sum;
			} else {
				if (!second.equals("*")) {
					int relFreq = sum;
					StringPair sp = new StringPair();
					sp.set(Integer.toString(relFreq), Double.toString(totalDOCs));
					StringIntPair sip = new StringIntPair();
					sip.set(key.getFirst(), Integer.parseInt(key.getSecond()));
					context.write(sip, sp);
				} else {
					//this gives us the term frequency of term in Doci
					// for a special key, we only record its value, for further
					// computation
					curMarginal = sum;
				}
			}

		}
	}

	
	//===================================================================
	// IDF mapper
	//Reads from the temporary document and starts the count for same terms across other documents 
	//	..in the vocabulary
	//OUTPUTS: StringIntPair(customized StringPair class) along with Intwritable to "group-by" terms.
	public static class IDFMapper extends Mapper<Object, Text, StringIntPair, IntWritable> {


		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] lines = value.toString().split("\n");
			for (String line : lines) {
				StringTokenizer itr = new StringTokenizer(line, ",?\\ ");
				String term = itr.nextToken().toString();

				String docID = itr.nextToken().toString();
				String tf = itr.nextToken().toString();

				StringIntPair sip = new StringIntPair();
				sip.set(term, Integer.parseInt(docID));
				context.write(sip, new IntWritable(Integer.parseInt(tf)));
				sip.set(term, -9000);
				context.write(sip, one);

			}
		}
	}

	//PARTITIONER
	// gets a haskKey based only on the TERM so that same terms go to same REDUCERS
	public static class IDFPartitioner extends Partitioner<StringIntPair, IntWritable> {

		public int getPartition(StringIntPair key, IntWritable value, int numPartitions) {

			return (key.getFirst().hashCode() & Integer.MAX_VALUE) % numPartitions;
		}

	}

	//FINAL IDF REDUCER
	//"gets()" conf and total N documents, aggregates terms across all documents and calculates tf*Log10(N/n)
	public static class IDFReducer extends Reducer<StringIntPair, IntWritable, Text, Text> {
		private DoubleWritable result = new DoubleWritable();
		private double curMarginal = 0;
		private double termInDocs = 0.0;

		public void reduce(StringIntPair key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;

			Configuration conf = context.getConfiguration();
			String TOTAL_DOCS = conf.get("TOTALnLINES");
			double TOTAL_DOCS_DOUBLE = Double.parseDouble(TOTAL_DOCS);
			for (IntWritable val : values) {
				sum += val.get();

			}

			String first = key.getFirst();
			int second = key.getSecond();

			if (second != -9000) {
				double ni = termInDocs;

				double tf_d = (double) sum;

				double weight = tf_d * (Math.log10(TOTAL_DOCS_DOUBLE / termInDocs));
				
				String right = Double.toString(weight);
				Text rightPart = new Text();
				rightPart.set(right);

				StringIntPair sp = new StringIntPair();
				sp.set(first.toString(), second);

				String leftPart = first.toString() + "\t" + Integer.toString(second);
				Text left_part = new Text();
				left_part.set(leftPart);
				context.write(left_part, rightPart);
			} else {
				// for a special key, we only record its value, for further
				// computation
				termInDocs = sum;
			}

		}
	}

	
	//The MAIN FUNCTION of PROJECT1
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		FileSystem fs = FileSystem.get(new URI(args[0]), conf);

		Path pt = new Path(new URI(args[0]));

		FileStatus[] listOfFiles = fs.listStatus(pt);

		int count = 0;

		//THIS LOOPS THROUGH THE FILE TO GET TOTAL DOCUMENTS
		for (FileStatus f : listOfFiles) {
			if (f.isFile()) {
				FSDataInputStream inputStream = fs.open(f.getPath());
				BufferedReader breader = new BufferedReader(new InputStreamReader(inputStream));
				String line = breader.readLine();
				while (line != null) {
					count++;
					line = breader.readLine();
				}
				if (breader != null) {
					breader.close();
				}
			}
		}

		//sets total N Docs
		conf.set("TOTALnLINES", Integer.toString(count));

		//changes the normal "\t" space b/w Key/Value so secondary sort can match the desired output
		conf.set("mapreduce.output.textoutputformat.separator", ",");

		Job job = Job.getInstance(conf, "Project 1 tf idf");
		job.setJarByClass(Project1.class);
		job.setMapperClass(TFMapper.class);
		job.setMapOutputKeyClass(StringPair.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setCombinerClass(TFCombiner.class);
		job.setReducerClass(TFReducer.class);
		job.setPartitionerClass(TFPartitioner.class);
		job.setOutputKeyClass(StringIntPair.class);
		job.setOutputValueClass(StringPair.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		String outputPath = args[1] + "/../tmp/";
		FileOutputFormat.setOutputPath(job, new Path(outputPath));
		//FIRST TF Part is written to a tmp file first
		job.waitForCompletion(true);

		// PART 2
		job = Job.getInstance(conf, "Part 2");
		job.setJarByClass(Project1.class);
		job.setMapperClass(IDFMapper.class);
		job.setMapOutputKeyClass(StringIntPair.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setReducerClass(IDFReducer.class);
		job.setPartitionerClass(IDFPartitioner.class);
		job.setNumReduceTasks(Integer.parseInt(args[2]));
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.addInputPath(job, new Path(outputPath));
		String outputPath2 = args[1];
		FileOutputFormat.setOutputPath(job, new Path(outputPath2));
				job.waitForCompletion(true);

		fs.delete(new Path(outputPath), true);

		// System.exit(job.waitForCompletion(true) ? 0 : 1);

	}
}
