// author: Manish Manandhar

package comp9313.proj1;

//modified StringPair.java

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.io.Writable;

public class StringIntPair implements WritableComparable<StringIntPair> {

	private String first;
	private int second;

	public StringIntPair() {
	}

	public StringIntPair(String first, int second) {
		set(first, second);
	}

	public void set(String left, int right) {
		first = left;
		second = right;
	}

	public String getFirst() {
		return first;
	}

	public int getSecond() {
		return second;
	}

	public void readFields(DataInput in) throws IOException {
		//String[] strings = WritableUtils.readStringArray(in);
//		first = strings[0];
		first = WritableUtils.readString(in);//
//		second = Integer.parseInt(strings[1]);
		second = WritableUtils.readVInt(in);//
	}

	
	public void write(DataOutput out) throws IOException {	
		//out.writeChars(first);
		//out.writeInt(second);
		
		WritableUtils.writeString(out, first);
		WritableUtils.writeVInt(out, second);


	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(first + " " + second);
		return sb.toString();
	}

//	@Override
//	public boolean equals(Object o) {
//		if (this == o)
//			return true;
//		if (o == null || getClass() != o.getClass())
//			return false;
//
//		StringPair that = (StringPair) o;
//
////		if (first != null ? !first.equals(that.first) : that.first != null)
////			return false;
////		if (second != null ? !second.equals(that.second) : that.second != null)
////			return false;
//		
//		return true;
//	}

	@Override
	public int hashCode() {
		//int result = first != null ? first.hashCode() : 0;
		//result = 31 * result + (second) != null ? second.hashCode() : 0);
		//return result;
		return first.hashCode();
	}
	
	private int compare(String s1, String s2){
		if (s1 == null && s2 != null) {
			return -1;
		} else if (s1 != null && s2 == null) {
			return 1;
		} else if (s1 == null && s2 == null) {
			return 0; 
		} else {
			return s1.compareTo(s2);
		}
	}
		
	//this function compares two integers
	private int compareINT(int n1, int n2) {
		if (n1 < n2) {
			return -1;
		} else {
			if (n2 == n1) {
				return 0;
			}else {
				return 1;
			}
		}
	}
	
	@Override
	public int compareTo(StringIntPair o) {
		int cmp = compare(first, o.getFirst()); 
		if(cmp != 0){
			return cmp;
		}
		return compareINT(second,o.getSecond());
	}	
	

}

