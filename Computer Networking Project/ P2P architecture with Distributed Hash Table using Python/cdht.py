# python 3.6
# 3 input arguments during execution from terminal
# then take inputs from user , like "request 0000" and "quit"

import sys, os
from socket import *
from time import sleep, time
import threading
from collections import deque

global peer
peer = int(sys.argv[1])
succesor = int(sys.argv[2])
succesors_succesor= int(sys.argv[3])
hostAddr = "127.0.0.1"

my_port = 50000+peer
succesor_port =  50000+succesor
succesors_succesor_port = 50000+ succesors_succesor

global predecesor_1
global predecesor_2
global succesor_1
global succesor_2
global breakThread1
global breakThread1_2
global breakThread2

global is_successor_1_dead
is_successor_1_dead = False


breakThread1 = False
breakThread1_2 = False
breakThread2 = False
predecesor_1 = -1
predecesor_2 = -1
succesor_1 = succesor
succesor_2 = succesors_succesor

sleep(2)


class Socket_Recv_Thread(threading.Thread):
	def __init__(self,peer, succesor,succesors_succesor,client):
		super(Socket_Recv_Thread,self).__init__()
		global predecesor_1
		global predecesor_2
		global succesor_1
		global succesor_2
		self.peer = peer
		self.succesor = succesor_1
		self.succesors_succesor = succesor_2
		self.client = client
		self.hostAddr = "127.0.0.1"
		self.my_port = 50000+self.peer
		self.succesor_port =  50000+self.succesor
		self.succesors_succesor_port = 50000 + self.succesors_succesor
		self.predecesor_1 = predecesor_1
		self.predecesor_2 = predecesor_2
	
	def send_ping(self,client, message, destination_port,hostAddr):
		client.sendto(message.encode(),(hostAddr,destination_port))

	def set_predecessor_s(self, pred_peer):
		global predecesor_1
		global predecesor_2
		if predecesor_1<0 and predecesor_2<0:
			#initial case
			predecesor_1= pred_peer
		elif predecesor_1>=0 and predecesor_2<0:
			#case when 1 predecessor is initialised
			if pred_peer != predecesor_1:
				predecesor_2 = pred_peer
				a = min(predecesor_1,predecesor_2)
				b = max(predecesor_1,predecesor_2)
				predecesor_1 = b
				predecesor_2 = a

				if (self.peer > predecesor_1 and peer < predecesor_2)\
				or (self.peer < predecesor_1 and self.peer > predecesor_2):#case 3 has predecessor 1 and 15
					a = min(predecesor_1,predecesor_2)
					b = max(predecesor_1,predecesor_2)
					predecesor_1 = a
					predecesor_2 = b
		elif predecesor_1>=0 and predecesor_2>=0:
			if (self.peer > predecesor_1 and self.peer < predecesor_2)\
			or (self.peer < predecesor_1 and self.peer > predecesor_2):#case
				a = min(predecesor_1,predecesor_2)
				b = max(predecesor_1,predecesor_2)
				predecesor_1 = a
				predecesor_2 = b

		return predecesor_1,predecesor_2		
		
	def bad_depart_send_TCP(self,to_peer,num,message_1 = "", message_2 = "",dead_peer = ""):
		global peer
		global predecesor_1
		global predecesor_2
		tcp_socket = socket(AF_INET,SOCK_STREAM)
		tcp_socket.connect((hostAddr,50000+to_peer))
		# tcp_socket.settimeout(1)
		send_dic = {}
		if num == 1:#for predecessor 1
			send_dic["type"] = "abrupt kill so send me your succesor_1 to set as my successor_2"
			send_dic["predecesor_1"] = peer
			send_dic["predecesor_2"] = predecesor_1
			send_dic["message_1"] = message_1
			send_dic["message_2"] =message_2
			send_dic["dead_peer"] = dead_peer
			tcp_socket.send(str(send_dic).encode())

		
	def run(self):#receive_ping_messages(self):
		global predecesor_1
		global predecesor_2
		global succesor_1
		global succesor_2
		global breakThread1
		global is_successor_1_dead
		response_List = deque()
		while True:
			# if len(response_List) == 4:
			# 	response_List = []

			if (breakThread1==True):
				break
			try:
				# self.client.settimeout(2)
				c_msg, addr = self.client.recvfrom(1024)  # Establish connection with client.
				c_msg = c_msg.decode()
				if c_msg[0:12] == "ping request":
					print("A ping request message was received from Peer",addr[1]-50000)
					message = "ping response from Peer "+ str(peer)
					predecesor_1,predecesor_2 = self.set_predecessor_s(addr[1]-50000)
					# sending response of the request
					self.send_ping(self.client,message,addr[1],self.hostAddr)

				elif c_msg[0:13] == "ping response":
					print("A ping response message was received from Peer",addr[1]-50000)
					
					if len(response_List)<4:
						response_List.append(addr[1]-50000)
					else:
						response_List.popleft()
						response_List.append(addr[1]-50000)

					if len(response_List)==4 and len(set(response_List))==1:
						if response_List[0] == succesor_1:
							# print("Peer", succesor_2, "is no longer alive.")
							# print("My first successor is now peer ", succesor_1)
							#find second successor
							pass
						else:
							message_1 = "Peer " + str(succesor_1)+ " is no longer alive."
							message_2 = "My first successor is now peer "+ str(succesor_2)
							dead_peer = str(succesor_1)
							is_successor_1_dead = True
							if is_successor_1_dead == True:
								succesor_1 = succesor_2
								# succesor_2 = -1
								self.bad_depart_send_TCP(succesor_1,1,message_1,message_2,dead_peer)
								is_successor_1_dead = False
						response_List = deque([])

			except OSError:
				pass
				# print("OSError: Receiving Thread")

class Socket_UDP_Send_Thread(threading.Thread):
	def __init__(self,client):
		super(Socket_UDP_Send_Thread,self).__init__()
		global predecesor_1
		global predecesor_2
		global succesor_1
		global succesor_2
		global peer 
		self.peer = peer
		self.succesor = succesor_1
		self.succesors_succesor = succesor_2
		self.client = client
		self.hostAddr = "127.0.0.1"
		self.my_port = 50000+peer
		self.succesor_port =  50000+succesor_1
		self.succesors_succesor_port = 50000 + succesor_2
		self.predecesor_1 = predecesor_1
		self.predecesor_2 = predecesor_2
	
	def send_ping(self,client, message, destination_port,hostAddr):
		try:
			client.sendto(message.encode(),(hostAddr,destination_port))
		except OSError:
			pass
			print("OSError: Sending Thread 0")

	#sleeps every 15 seconds
	def run(self):#receive_ping_messages(self):
		global predecesor_1
		global predecesor_2
		global succesor_1
		global succesor_2
		global peer 
		global breakThread1_2
		while True:
			if (breakThread1_2):
				break
			ping_request_message = "ping request Peer " + str(peer)
			message = ping_request_message
			# self.send_ping(self.client,ping_request_message,50000+succesor_1,self.hostAddr)
			
			# self.client.settimeout(2)
			self.client.sendto(message.encode(),(self.hostAddr,50000+succesor_1))
			self.client.sendto(message.encode(),(self.hostAddr,50000+succesor_2))

			sleep(10)

client = socket(AF_INET,SOCK_DGRAM)
client.setsockopt(SOL_SOCKET, SO_REUSEADDR,1)
client.bind((hostAddr,my_port))
# client.settimeout(2) ##when we fail to receive aknowledement from 

thread1 = Socket_Recv_Thread(peer,succesor,succesors_succesor,client)
thread1.start()

thread1_2 = Socket_UDP_Send_Thread(client)
thread1_2.start()

# client.close()

#TCP
class TCP_socket(threading.Thread):
	def __init__(self, thread1):
		super(TCP_socket,self).__init__()
		global predecesor_1
		global predecesor_2
		global succesor_1
		global succesor_2
		global peer
		self.server =  socket(AF_INET,SOCK_STREAM) #tcp server
		self.server.setsockopt(SOL_SOCKET, SO_REUSEADDR,1)
		self.server.bind(('',thread1.my_port))
		self.server.listen(5)
		self.peer = peer
		self.succesor = succesor_1
		self.succesors_succesor = succesor_2
		self.predecesor_1 = predecesor_1
		self.predecesor_2 = predecesor_2
		self.hostAddr = "127.0.0.1"

	def hash_function_2(self,file_number):
		return file_number%256
# inside TCP rec thread
	def send_TCP_msg_2(self,file_number,peer_asked):
		tcp_socket = socket(AF_INET,SOCK_STREAM)
		tcp_socket.connect((self.hostAddr,50000+succesor_1))
		send_str = "Do you have file "+ str(file_number)
		send_dic = {}
		send_dic["peer_asked"] = peer_asked
		send_dic["type"] = "file request"
		send_dic["file_number"] = str(file_number)
		tcp_socket.send(str(send_dic).encode())

	def send_response(self,file_number,peer_asked):
		tcp_socket = socket(AF_INET,SOCK_STREAM)
		tcp_socket.connect((self.hostAddr,50000+int(peer_asked)))
		send_str = "Received a response message from peer " + str(self.peer) + " which has the file " +str(file_number)
		send_dic = {}
		send_dic["peer_asked"] = peer_asked
		send_dic["type"] = "file response"
		send_dic["file_number"] = str(file_number)
		send_dic["message"] = send_str
		tcp_socket.send(str(send_dic).encode())

	def abrupt_kills_msgs(self, send_dic, to_peer_port):
		tcp_socket = socket(AF_INET,SOCK_STREAM)
		tcp_socket.connect((self.hostAddr,50000+to_peer_port))
		tcp_socket.send(str(send_dic).encode())

	def run(self):
		global predecesor_1
		global predecesor_2
		global succesor_1
		global succesor_2
		global peer
		global breakThread2
		global is_successor_1_dead
		while True:
			if (breakThread2):
				break
			connectionSocket, addr = self.server.accept()
			received_msg = connectionSocket.recv(1024)
			received_msg = received_msg.decode()
			received_msg = eval(received_msg)
			if (received_msg["type"]=="file request"):
				file_number = int(received_msg["file_number"])
				file_code = self.hash_function_2(file_number)
				peer_asked = received_msg["peer_asked"]
				if file_code == peer:
					print("File",file_number,"is here. ")
					print("A response message, destined for peer",peer_asked,", has been sent")
					self.send_response(file_number,peer_asked)
				else:
					if predecesor_1>succesor_1 and predecesor_1>succesor_2 and succesor_1<peer: #case for node 15 only
						if file_code > peer:
							print("File",file_number,"is not stored here.")
							print("File request message has been sent to my successor.")
							self.send_TCP_msg_2(file_number,peer_asked)
						elif file_code<peer and file_code>predecesor_1:
							print("File",file_number,"is here.")
							print("A response message, destined for peer",peer_asked,", has been sent")
							self.send_response(file_number,peer_asked)
						elif file_code < peer and file_code>=succesor_1:
							print("File",file_number,"is not stored here.")
							print("File request message has been sent to my successor.")
							self.send_TCP_msg_2(file_number,peer_asked)
						else:
							print("File",file_number,"is here.")
							print("A response message, destined for peer",peer_asked,", has been sent")
							self.send_response(file_number,peer_asked)
					elif peer<predecesor_1 and peer<succesor_1:#case for first node only
						if file_code>predecesor_1:
							print("File",file_number,"is here.")
							print("A response message, destined for peer",peer_asked,", has been sent")
							self.send_response(file_number,peer_asked)
						elif file_code <= peer:
							print("File",file_number,"is here.")
							print("A response message, destined for peer",peer_asked,", has been sent")
							self.send_response(file_number,peer_asked)
						elif file_code >= succesor_1 and file_code > peer:
							print("File",file_number,"is not stored here.")
							print("File request message has been sent to my successor.")
							self.send_TCP_msg_2(file_number,peer_asked)
						elif file_code < peer and file_code < succesor_1:
							print("File",file_number,"is not stored here.")
							print("File request message has been sent to my successor.")
							self.send_TCP_msg_2(file_number,peer_asked)
						else:
							print("File",file_number,"is here.")
							print("A response message, destined for peer",peer_asked,", has been sent")
							self.send_response(file_number,peer_asked)
					else: #case for all other nodes apart from 15
					# for all these node, the succesor is bigger
						# if file_code <= peer and file_code < succesor_1:
						if file_code in range(predecesor_1+1,peer):
							print("File",file_number,"is here.")
							print("A response message, destined for peer",peer_asked,", has been sent")
							self.send_response(file_number,peer_asked)
						elif file_code < peer and file_code < succesor_1:
							print("File",file_number,"is not stored here.")
							print("File request message has been sent to my successor.")
							self.send_TCP_msg_2(file_number,peer_asked)
						else:
							print("File",file_number,"is not stored here.")
							print("File request message has been sent to my successor.")
							self.send_TCP_msg_2(file_number,peer_asked)
			elif (received_msg["type"]=="file response"):
				print(received_msg["message"])
			elif (received_msg["type"]=="update both successor"):
				print(received_msg["message"])
				succesor_1 = int(received_msg["succesor_1"])
				succesor_2 = int(received_msg["succesor_2"])
				print("My first successor is now Peer",succesor_1)
				print("My second successor is now Peer",succesor_2)
			elif (received_msg["type"]=="update succesor_2"):
				print(received_msg["message"])
				succesor_2 = int(received_msg["succesor_2"])
				print("My first successor is now Peer",succesor_1)
				print("My second successor is now Peer",succesor_2)
			elif (received_msg["type"]=="update both predecesors"):
				predecesor_1 = int(received_msg["predecesor_1"])
				predecesor_2 = int(received_msg["predecesor_2"])
			elif (received_msg["type"]=="update predecesor_2"):
				predecesor_2 = int(received_msg["predecesor_2"])
			elif (received_msg["type"]=="abrupt kill so send me your succesor_1 to set as my successor_2"):
				predecesor_1 = int(received_msg["predecesor_1"])
				predecesor_2 = int(received_msg["predecesor_2"])
				send_dic = {}
				send_dic["type"] = "response for abrupt kill so send me your succesor_1 to set as my successor_2"
				send_dic["succesor_1"] = succesor_1
				send_dic["message_1"] = received_msg["message_1"]
				send_dic["message_2"] =  received_msg["message_2"]
				send_dic["dead_peer"] = received_msg["dead_peer"]
				self.abrupt_kills_msgs(send_dic,predecesor_1)
			elif (received_msg["type"]=="response for abrupt kill so send me your succesor_1 to set as my successor_2"):
				succesor_2 = int(received_msg["succesor_1"])
				# print("HERE==================================================")
				print(received_msg["message_1"])
				print(received_msg["message_2"])
				print("My second successor is now peer",succesor_2)
				
				#update predecessors of the dead peer's successor_2
				send_dic = {}
				send_dic["type"] = "abrupt kill, so succesor_2 changes predecessors"
				send_dic["predecesor_1"] = succesor_1
				send_dic["predecesor_2"] = peer
				self.abrupt_kills_msgs(send_dic,succesor_2)

				#update successors of predecessor_2 of dead peer

				send_dic2 = {}
				send_dic2["type"] = "abrupt kill, update both successors"
				send_dic2["succesor_1"] = peer
				send_dic2["succesor_2"] = succesor_1
				send_dic2["message_1"] = "Peer "+ str(received_msg["dead_peer"]) + " is no longer alive"
				send_dic2["message_2"] = "My first successor is now peer " + str(peer)
				send_dic2["message_3"] = "My second successor is now peer " + str(succesor_1)
				self.abrupt_kills_msgs(send_dic2,predecesor_1)

			elif (received_msg["type"] == "abrupt kill, so succesor_2 changes predecessors"):
				predecesor_1 = int(received_msg["predecesor_1"])
				predecesor_2 = int(received_msg["predecesor_2"])
			elif(received_msg["type"]=="abrupt kill, update both successors"):
				succesor_1 = int(received_msg["succesor_1"])
				succesor_2 = int(received_msg["succesor_2"])
				print(received_msg["message_1"])
				print(received_msg["message_2"])
				print(received_msg["message_3"])


thread2 = TCP_socket(thread1)
thread2.start()

def hash_function(file_number):
	return file_number%256

#in main thread
def send_TCP_msg(file_number):
	tcp_socket = socket(AF_INET,SOCK_STREAM)
	tcp_socket.connect((hostAddr,succesor_port))
	send_str = "Do you have file "+ str(file_number)
	send_dic = {}
	send_dic["peer_asked"] = str(peer)
	send_dic["type"] = "file request"
	send_dic["file_number"] = str(file_number)
	tcp_socket.send(str(send_dic).encode())

def good_departure_send_TCP(to_peer,num):
	try:
		tcp_socket = socket(AF_INET,SOCK_STREAM)
		tcp_socket.connect((hostAddr,50000+to_peer))
		tcp_socket.settimeout(1)
		send_dic = {}
		if num == 1:#for predecessor 1
			send_dic["type"] = "update both successor"
			send_dic["message"] = "Peer "+str(peer)+" will depart from Network."
			send_dic["succesor_1"] = succesor_1
			send_dic["succesor_2"] = succesor_2
			tcp_socket.send(str(send_dic).encode())
		elif num == 2:#for predecessor 2
			send_dic["type"] = "update succesor_2"
			send_dic["message"] = "Peer "+str(peer)+" will depart from Network."
			send_dic["succesor_2"] = succesor_1
			tcp_socket.send(str(send_dic).encode())
			# print("1>>TCP DATA TO,"predecesor_2," sent")
		elif num ==3:#for successor 1
			send_dic["type"] = "update both predecesors"
			send_dic["predecesor_1"] = predecesor_1
			send_dic["predecesor_2"] = predecesor_2
			tcp_socket.send(str(send_dic).encode())
		elif num ==4:#for successor 2
			send_dic["type"] = "update predecesor_2"
			send_dic["predecesor_2"] = predecesor_1
			tcp_socket.send(str(send_dic).encode())
		return True
	except ConnectionRefusedError:
		return False



#this loops only takes inputs
#and sends only when the user inputs "quit" or "request XXXX"
while True:

	

	# send a message to successor 1 about its succesor and make it successor 2

	try:
		text_input = input()
		is_good_input  = True
		if text_input[0:7]=="request" and len(text_input)==12:
			# print("I HAVE predecessors: ",thread1.predecesor_1,thread1.predecesor_2, predecesor_1,predecesor_2)
			try:
				file_number = int(text_input[-4:])
				file_code = hash_function(file_number)

				if file_code == peer:
					print("File",text_input[-4:],"is here.")
				else:
					if predecesor_1>succesor_1 and predecesor_1>succesor_2 and succesor_1<peer: #case for node 15 only
						if file_code > peer:
							print("File request message for ",file_number," has been sent to my successor.")
							send_TCP_msg(text_input[-4:])

						elif file_code<peer and file_code>predecesor_1:
							print("File",file_number,"is here")

						elif file_code < peer and file_code > succesor_1:
							print("File request message for ",file_number," has been sent to my successor.")
							send_TCP_msg(text_input[-4:])
						else:
							print("File",text_input[-4:],"is here.")
					elif peer<predecesor_1 and peer<succesor_1:#case for first node only
						if file_code>predecesor_1:
							print("File",text_input[-4:],"is here.")
						elif file_code <= peer:
							print("File",text_input[-4:],"is here.")
						elif file_code >= succesor_1 and file_code > peer:
							print("File request message for ",file_number," has been sent to my successor.")
							send_TCP_msg(text_input[-4:])
						elif file_code < peer and file_code < succesor_1:
							print("File request message for ",file_number," has been sent to my successor.")
							send_TCP_msg(text_input[-4:])
						else:
							print("File",text_input[-4:],"is here.")
					else: #case for all other nodes apart from 15
					# for all these node, the succesor is bigger
						if file_code in range(predecesor_1+1,peer):
						#if file_code <= peer and file_code < succesor_1:#file_code >= succesor_1 and file_code > peer:
							print("File",text_input[-4:],"is here.")
						elif file_code < peer and file_code < succesor_1:
							print("File request message for ",file_number," has been sent to my successor.")
							send_TCP_msg(text_input[-4:])
						else:
							print("File request message for ",file_number," has been sent to my successor.")
							send_TCP_msg(text_input[-4:])
			except ValueError:
					is_good_input = False
					print("ValueError: Invalid file request")

		elif text_input =="quit":#for graceful exit
	# 10 informs predecessors 8,5 gives them new succer
			p1_ok = good_departure_send_TCP(predecesor_1,1)
			p2_ok = good_departure_send_TCP(predecesor_2,2)
			s1_ok = good_departure_send_TCP(succesor_1,3)
			s2_ok = good_departure_send_TCP(succesor_2,4)

			breakThread1 = True
			breakThread1_2 = True
			breakThread2 = True
			break
		
		elif text_input == "status": #for debugging purposes
			print("====================================================")
			print("successors: ", succesor_1,succesor_2, is_successor_1_dead)
			print("predecesors: ", predecesor_1,predecesor_2)
			print("====================================================")

		

	except KeyboardInterrupt:
		print("\nKeyboard Interrupted Quit!")
		breakThread1 = True
		breakThread1_2 = True
		breakThread2 = True
		# thread1.join()
		# thread1_2.join()
		# thread2.join()
		# socket.close()
		# client.close()
		break

os._exit(1)

# request 2012 at 8, at 1
# request 0006 at 8, at 8
