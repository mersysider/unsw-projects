import torch
import numpy as np
from config import config
import torch.nn.functional as F

_config = config()


# helper for evaluate()
# returns sequences of I-TAR,I-HYP for a particular B-TAR/B-HYP
def get_Itar_or_Ihyp_sequence(remaining_list,next_item):
#     print("remaining_list", remaining_list)
    new_list = list()
    new_list.append(remaining_list[0])
    for i in range(1,len(remaining_list)):
        new_term = remaining_list[i]
        if new_term==next_item:
            new_list.append(new_term)
        else:
            # break when no IHYP or ITAR is found
            break
    return tuple(new_list)
    
def evaluate(golden_list, predict_list):
    
    gt_set = set()
    pr_set = set()
    #goes through each sentence in golden_list and  predict_list
    for sentence_ID in range(len(golden_list)): 
        gt_sentence = golden_list[sentence_ID]
        pr_sentence = predict_list[sentence_ID]
        # for tp calculation
        for seq_ID in range(len(gt_sentence)):
            # when word is B-*, we knw there a label sequence so start searching for it suing the helper function
            if gt_sentence[seq_ID] == "B-TAR":
                i_list_gt = get_Itar_or_Ihyp_sequence(gt_sentence[seq_ID:],"I-TAR")
                gt_set.add(((sentence_ID,seq_ID),i_list_gt))
            elif gt_sentence[seq_ID] == "B-HYP":
                i_list_gt = get_Itar_or_Ihyp_sequence(gt_sentence[seq_ID:],"I-HYP")
                gt_set.add(((sentence_ID,seq_ID),i_list_gt))
                
            # when word is B-*, we knw there a label sequence so start searching for it suing the helper function
            if pr_sentence[seq_ID] == "B-TAR":
                i_list_pr = get_Itar_or_Ihyp_sequence(pr_sentence[seq_ID:],"I-TAR")
                pr_set.add(((sentence_ID,seq_ID),i_list_pr))
            elif pr_sentence[seq_ID] == "B-HYP":
                i_list_pr = get_Itar_or_Ihyp_sequence(pr_sentence[seq_ID:],"I-HYP")
                pr_set.add(((sentence_ID,seq_ID),i_list_pr))
                
    recall_denominator = 0.0
    precision_denominator = 0.0
    
    #true positive count
    tp = len(gt_set.intersection(pr_set))

    # prevents dividing by zero
    if tp==0:
        F1 = 0.000
    else:
        recall_denominator = len(gt_set)
        precision_denominator = len(pr_set)
        #precision
        P = tp/precision_denominator
        #Recall
        R = tp/recall_denominator
        
        # prevents dividing by zero
        if (P+R) == 0:
            F1 = 0.000
        else:
            # harmonic mean of P and R
            F1 = 2*P*R/(P+R)
    return F1


def new_LSTMCell(input, hidden, w_ih, w_hh, b_ih=None, b_hh=None):
    hx, cx = hidden
    gates = F.linear(input, w_ih, b_ih) + F.linear(hx, w_hh, b_hh)
    ingate, forgetgate, cellgate, outgate = gates.chunk(4, 1)
    ingate = torch.sigmoid(ingate)
    forgetgate = torch.sigmoid(forgetgate)
    cellgate = torch.tanh(cellgate)
    outgate = torch.sigmoid(outgate)
    cy = (forgetgate * cx) + ((1-forgetgate) * cellgate) 
    hy = outgate * torch.tanh(cy)

    return hy, cy

import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

def get_char_sequence(model, batch_char_index_matrices, batch_word_len_lists):

    # dimensions:
    # batch_char_index_matrices[10, 7, 11]
    # batch_word_len_lists[10, 7]
    # input_char_embeds[10, 7, 11, 50]

    # VERSION 1 is simpler and faster implementation

    # ------------------------Version 1------------------------
    # get char embeddings vector
    input_char_embeds = model.char_embeds(batch_char_index_matrices)
    dims = list(input_char_embeds.size())

    # convert embeddings to [70,...,...]
    input_char_embeds = input_char_embeds.view(-1,dims[2],dims[3])
    batch_word_len_lists = batch_word_len_lists.view(-1)
    
    # sort embeddings based on length
    perm_idx, sorted_batch_word_len_lists = model.sort_input(batch_word_len_lists)
    sorted_input_embeds = input_char_embeds[perm_idx]
    _, desorted_indices = torch.sort(perm_idx, descending=False)

    # pack the sequence of embeddings
    output_sequence = pack_padded_sequence(sorted_input_embeds, lengths=sorted_batch_word_len_lists.data.tolist(), batch_first=True)
    output_sequence, state = model.char_lstm(output_sequence) # [2,70,50]
    hidden_state = state[0]

    # concat both hidden states of forward layer and backward layer [70,100]
    combined_state = torch.cat([hidden_state[0],hidden_state[1]],dim=-1)

    # unsort
    output_sequence = combined_state[desorted_indices]

    #convert back to [10,7,100]
    output_sequence = output_sequence.view(dims[0],dims[1],-1)
    return output_sequence

    # ---------------------Version 2 using loop but slower--------------------
    # input_char_embeds = model.char_embeds(batch_char_index_matrices)
    # dims = list(input_char_embeds.size())
    # final_concat_matrix = torch.zeros(dims[0],dims[1],dims[3]*2)
    # for sen_idx in range(len(input_char_embeds)):
    #     perm_idx, sorted_batch_word_len_lists = model.sort_input(batch_word_len_lists[sen_idx])
    #     sorted_input_embeds = input_char_embeds[sen_idx][perm_idx]
    #     _, desorted_indices = torch.sort(perm_idx, descending=False)
    #     output_sequence = pack_padded_sequence(sorted_input_embeds, lengths=sorted_batch_word_len_lists.data.tolist(), batch_first=True)
    #     output_sequence, state = model.char_lstm(output_sequence)
    #     hidden_state = state[0]
    #     combined_state = torch.cat([hidden_state[0],hidden_state[1]],dim=-1)
    #     output_sequence = combined_state[desorted_indices]
    #     final_concat_matrix[sen_idx] = output_sequence
    # return final_concat_matrix
    # ------------------------------------
