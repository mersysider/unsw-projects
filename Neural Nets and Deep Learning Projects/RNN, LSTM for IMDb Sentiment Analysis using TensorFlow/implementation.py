# author: MANISH MANANDHAR

import tensorflow as tf
import numpy as np
import re

BATCH_SIZE = 32
MAX_WORDS_IN_REVIEW = 200  # Maximum length of a review to consider
EMBEDDING_SIZE = 50  # Dimensions for each word vector

stop_words = set({'ourselves', 'hers', 'between', 'yourself', 'again',
                  'there', 'about', 'once', 'during', 'out', 'very', 'having',
                  'with', 'they', 'own', 'an', 'be', 'some', 'for', 'do', 'its',
                  'yours', 'such', 'into', 'of', 'most', 'itself', 'other',
                  'off', 'is', 's', 'am', 'or', 'who', 'as', 'from', 'him',
                  'each', 'the', 'themselves', 'below', 'are', 'we',
                  'these', 'your', 'his', 'through', 'don', 'me', 'were',
                  'her', 'more', 'himself', 'this', 'down', 'should', 'our',
                  'their', 'while', 'above', 'both', 'up', 'to', 'ours', 'had',
                  'she', 'all', 'no', 'when', 'at', 'any', 'before', 'them',
                  'same', 'and', 'been', 'have', 'in', 'will', 'on', 'does',
                  'yourselves', 'then', 'that', 'because', 'what', 'over',
                  'why', 'so', 'can', 'did', 'not', 'now', 'under', 'he', 'you',
                  'herself', 'has', 'just', 'where', 'too', 'only', 'myself',
                  'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being',
                  'if', 'theirs', 'my', 'against', 'a', 'by', 'doing', 'it',
                  'how', 'further', 'was', 'here', 'than'})

def preprocess(review):
    """
    Apply preprocessing to a single review. You can do anything here that is manipulation
    at a string level, e.g.
         removing stop words
        - stripping/adding punctuation
         changing case
        - word find/replace
    RETURN: the preprocessed review in string form.
    """
    

    review = review.lower().replace("<br />"," ")
    review = review.replace("<br>"," ")
    review = review.replace("</br>"," ")
    # filters non aphabets and numbers
    regex = re.compile("[^A-Za-z0-9 ]+")
    review = re.sub(regex, "", review.lower())
    
    reviewWords = review.split(" ")
    processed_review = list()
    count = 0
    for word in reviewWords:

      if count == MAX_WORDS_IN_REVIEW:
        break

      if word not in stop_words:
        if count==0 :
          processed_review.append(word)
          count += 1
        else:
          processed_review.append(word)
          count += 1

    return processed_review


def define_graph():
    """
    Implement your model here. 
    You will need to define placeholders, for the input and labels,
      Note that the input is not strings of words, but the strings after the embedding lookup
      has been applied (i.e. arrays of floats).

    In all cases this code will be called by an unaltered runner.py. You should read this
    file and ensure your code here is compatible.

    Consult the assignment specification for details of which parts of the TF API are
    permitted for use in this function.

    You must return, in the following order, the placeholders/tensors for;
    RETURNS: input, labels, optimizer, accuracy and loss
    """


    lstm_units = 64
    num_classes = 2  
    # input data is a tf placeholder with dimensions/shape of [32, 200, 50]
    input_data = tf.placeholder(tf.float32, [BATCH_SIZE, MAX_WORDS_IN_REVIEW, EMBEDDING_SIZE], name='input_data')

    # labels is a tf placeholder with shape [32, 2]
    labels = tf.placeholder(tf.int32, [BATCH_SIZE, num_classes], name='labels')

    # 0.6 as used in runner.py
    dropout_keep_prob = tf.placeholder_with_default(0.6, shape=())
    
    # basic LSTM cell with 64 hidden units
    lstmCell = tf.contrib.rnn.BasicLSTMCell(lstm_units)

    #wraps the LSTM to a dropout layer to avoid overfitting
    lstmCell = tf.contrib.rnn.DropoutWrapper(cell=lstmCell, output_keep_prob=dropout_keep_prob)

    # dynamic RNN helps data to flow through LSTM RNN
    value, _ = tf.nn.dynamic_rnn(lstmCell, input_data, dtype=tf.float32)

    # weight matrix
    weight = tf.Variable(tf.truncated_normal([lstm_units, num_classes]))
    # bias matrix
    bias = tf.Variable(tf.truncated_normal(shape=[num_classes]))

    # value returned from dynamic_rnn is reshaped
    value = tf.transpose(value, [1, 0, 2])
    last = tf.gather(value, int(value.get_shape()[0]) - 1)

    # final output prediction
    prediction = (tf.matmul(last, weight) + bias)
    
    # checks if the predicted label is the same as the actual label
    correctPred = tf.equal(tf.argmax(prediction, 1), tf.argmax(labels, 1))

    # reduce mean of casted correctedPred to calculate the accuracy
    accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32), name='accuracy')

    # softmax to get the loss
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=labels), name='loss')

    # Adam Optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate = 0.0001).minimize(loss)



    return input_data, labels, dropout_keep_prob, optimizer, accuracy, loss
