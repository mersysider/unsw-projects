import gym
import tensorflow as tf
import numpy as np
import random

#=====================================================
# FOR DEBUGGING
#=====================================================
# from datetime import datetime
# start=datetime.now()
#=====================================================



# General Parameters
# -- DO NOT MODIFY --
ENV_NAME = 'CartPole-v0'
EPISODE = 200000  # Episode limitation
STEP = 200  # Step limitation in an episode
TEST = 10  # The number of tests to run every TEST_FREQUENCY episodes
TEST_FREQUENCY = 100  # Num episodes to run before visualizing test accuracy

#=====================================================
# TODO: HyperParameters
#=====================================================
GAMMA = 0.98 # discount factor
INITIAL_EPSILON =  0.6# starting value of epsilon
FINAL_EPSILON =  0.01 # final value of epsilon
EPSILON_DECAY_STEPS =  2500# decay period

# added later
MAX_MEM=10000
BATCH_SIZE=24
HIDDEN_NODES = 50

#=====================================================
# EOF: HyperParameters
#=====================================================

# Create environment
# -- DO NOT MODIFY --
env = gym.make(ENV_NAME)
epsilon = INITIAL_EPSILON
STATE_DIM = env.observation_space.shape[0]
ACTION_DIM = env.action_space.n

# Placeholders
# -- DO NOT MODIFY --
state_in = tf.placeholder("float", [None, STATE_DIM])
action_in = tf.placeholder("float", [None, ACTION_DIM])
target_in = tf.placeholder("float", [None])

#=====================================================
# TODO: Define Network Graph
#=====================================================
# layer 1
W1 = tf.Variable(tf.truncated_normal([STATE_DIM ,HIDDEN_NODES]))
b1=tf.Variable(tf.truncated_normal([HIDDEN_NODES]))

# layer 2
# W2=tf.Variable(tf.truncated_normal([HIDDEN_NODES, HIDDEN_NODES]))
# b2=tf.Variable(tf.truncated_normal([HIDDEN_NODES]))

W2=tf.Variable(tf.truncated_normal([HIDDEN_NODES, ACTION_DIM]))
b2=tf.Variable(tf.truncated_normal([ACTION_DIM]))

logits_1= tf.nn.relu(tf.matmul(state_in,W1))+b1
# logits_2 = tf.nn.relu(tf.matmul(logits_1,W2))+b2

#=====================================================
# EOF: Define Network Graph
#=====================================================

#=====================================================
# TODO: Network outputs
#=====================================================
q_values = tf.matmul(logits_1,W2)+b2
q_action = tf.reduce_sum(tf.multiply(q_values,action_in), reduction_indices=1)
#=====================================================
# EOF: Network outputs
#=====================================================


#=====================================================
# TODO: Loss/Optimizer Definition
#=====================================================
loss = tf.reduce_mean(tf.square(target_in-q_action))
optimizer = tf.train.AdamOptimizer(learning_rate=0.005).minimize(loss)

# optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.005).minimize(loss)
# tried ADAM OPTIMIZER
#=====================================================
# EOF: Loss/Optimizer Definition
#=====================================================

# Start session - Tensorflow housekeeping
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())


# -- DO NOT MODIFY ---
def explore(state, epsilon):
    """
    Exploration function: given a state and an epsilon value,
    and assuming the network has already been defined, decide which action to
    take using e-greedy exploration based on the current q-value estimates.
    """
    Q_estimates = q_values.eval(feed_dict={
        state_in: [state]
    })
    if random.random() <= epsilon:
        action = random.randint(0, ACTION_DIM - 1)
    else:
        action = np.argmax(Q_estimates)
    one_hot_action = np.zeros(ACTION_DIM)
    one_hot_action[action] = 1
    return one_hot_action

def get_lists(batch):
    state_list = []
    action_list = []
    next_state_list = []
    for item in batch:
        state_list.append(item[0])
        action_list.append(item[1])
        next_state_list.append(item[2])
    return state_list, action_list,next_state_list

# for exp. replay
# a Database for experiences
batch_memory= list()

# Main learning loop
for episode in range(EPISODE):

    # initialize task
    state = env.reset()

    # Update epsilon once per episode
    epsilon -= epsilon / EPSILON_DECAY_STEPS
    
    # Move through env according to e-greedy policy
    for step in range(STEP):
        action = explore(state, epsilon)
        action_index = np.argmax(action)
        next_state, reward, done, _ = env.step(action_index)
        
        #gets the action row vector, with 1 in index of "np.argmax(action)""
        #  the (1) row vector is extracted with [0] in the end
        # SIMILAR TO ONE HOT ACTION from def explore(state, epsilon):
        current_action_2 = [0]*ACTION_DIM
        current_action_2[action_index] = 1


        # if list(action) == current_action_2:
        #     print(episode,step)

        # current_action_2 = action
        # print("current_action_2",current_action_2)
        batch_memory.append((state, current_action_2, next_state, reward, done))
        
        if len(batch_memory)> 2*BATCH_SIZE:
            if len(batch_memory)>MAX_MEM:
                #slice memory list, remove from left
                batch_memory = batch_memory[1:]
            batch= random.sample(batch_memory, BATCH_SIZE)
            state_list, action_list,next_state_list = get_lists(batch)
        
            nextstate_q_values = q_values.eval(feed_dict={
                state_in: next_state_list
            })

        # TODO: Calculate the target q-value.
        # hint1: Bellman
        # hint2: consider if the episode has terminated
            target = []
            
            for i in range(0,BATCH_SIZE):
                finish= batch[i][4] #False or True
                if finish:
                    # pass
                    target.append(reward)
                else:
                    target.append(reward+GAMMA*np.max(nextstate_q_values[i]))

            # Do one training step
            session.run([optimizer], feed_dict={
                target_in: target,
                action_in: action_list,
                state_in: state_list
            })

        # Update
        state = next_state
        if done:
            break
            
    # print(episode)

    # Test and view sample runs - can disable render to save time
    # -- DO NOT MODIFY --
    if (episode % TEST_FREQUENCY == 0 and episode != 0):
        total_reward = 0
        for i in range(TEST):
            state = env.reset()
            for j in range(STEP):
                env.render()
                action = np.argmax(q_values.eval(feed_dict={
                    state_in: [state]
                }))
                state, reward, done, _ = env.step(action)
                total_reward += reward
                if done:
                    break
        ave_reward = total_reward / TEST
        print('episode:', episode, 'epsilon:', epsilon, 'Evaluation '
                                                        'Average Reward:', ave_reward)
        

#=====================================================
# FOR DEBUGGING
#=====================================================
        # print(datetime.now()-start)
env.close()
