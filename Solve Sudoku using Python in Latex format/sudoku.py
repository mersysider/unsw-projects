# import pandas as pd
import sys
from collections import deque, defaultdict
import copy

class SudokuError(Exception):
    def __init__(self, message):
        self.message = message
#         sys.exit()

class Sudoku:
    def __init__(self,filename = ''):
        self.filename = filename
        self._check_if_wrong()
        self.init_latex_top_bottom()
        
    def _check_if_wrong(self):
        #funct is called initially, if sudoku text is valid, numbers only
#         9 rows, 9 columns, no A's or spaces
        with open(self.filename) as file:
            self.sudoku_grid = []
            for line in file:
                if line.lstrip() != "":
#                     print(line)
                    new_line = []
                    for i in line:
                        try:
                            int_i = int(i)
                            new_line.append(int_i)
                        except ValueError:
                            pass
                    if len(new_line) != 9:
                        raise SudokuError('Incorrect input')
                    else:
                        self.sudoku_grid.append(new_line)
        if len(self.sudoku_grid) != 9:
            raise SudokuError('Incorrect input')
    
    def _is_in_box(self,r,c,val,grid):
        #returns if a num "val" is in the same box, returns None for False
        grid = grid
        di={0:0,1:3,2:6}
        r_i = di[r // 3]
        c_i = di[c // 3]
        for i in range(r_i,r_i+3):
            for k in range(c_i,c_i+3):
                if grid[i][k] == val and i!=r and k!=c:
                    return True

    def _is_in_column(self,grid,val,r,c):
#         checks if a val is in column
        for i in range(9):
            if i !=r:
                if grid[i][c] == val:
                    return True
    
    def preassess(self):
        grid = self.sudoku_grid
        for r in range(9):
            for c in range(9):
                val = grid[r][c]
                if val>0:
                    is_in_box = self._is_in_box(r,c,val,grid)
                    is_in_row = False
                    if grid[r].count(val)>1:
                        is_in_row = True
                    is_in_column = self._is_in_column(grid,val,r,c)
                    if is_in_box or is_in_row or is_in_column:
                        return "There is clearly no solution."
                        
        return "There might be a solution."
    def init_latex_top_bottom(self):
        self.top_part = """\documentclass[10pt]{article}
\\usepackage[left=0pt,right=0pt]{geometry}
\\usepackage{tikz}
\\usetikzlibrary{positioning}
\\usepackage{cancel}
\\pagestyle{empty}

\\newcommand{\\N}[5]{\\tikz{\\node[label=above left:{\\tiny #1},
                               label=above right:{\\tiny #2},
                               label=below left:{\\tiny #3},
                               label=below right:{\\tiny #4}]{#5};}}

\\begin{document}

\\tikzset{every node/.style={minimum size=.5cm}}

\\begin{center}
\\begin{tabular}{||@{}c@{}|@{}c@{}|@{}c@{}||@{}c@{}|@{}c@{}|@{}c@{}||@{}c@{}|@{}c@{}|@{}c@{}||}\\hline\\hline\n"""

        self.bot_part = """\\end{tabular}
\\end{center}

\\end{document}"""
        
    def bare_tex_output(self,is_forced = False):
        grid = self.sudoku_grid
        mid_str = ""
        filename = self.filename
        for i in range(9):
            mid_str += f"% Line {i+1}\n"
            for j in range(0,3):
                a =""
                if grid[i][j]>0:
                    a=str(grid[i][j])
                if j ==2:
                    string_ele = '\\N{}{}{}{}{'+a+'} &'
                else:
                    string_ele = '\\N{}{}{}{}{'+a+'} & '
                mid_str += string_ele
            mid_str += "\n"
            for k in range(3,6):
                a =""
                if grid[i][k]>0:
                    a=str(grid[i][k])
                if k ==5:
                    string_ele = '\\N{}{}{}{}{'+a+'} &'
                else:
                    string_ele = '\\N{}{}{}{}{'+a+'} & '
                mid_str += string_ele
            mid_str += "\n"
            for l in range(6,9):
                a =""
                if grid[i][l]>0:
                    a=str(grid[i][l])
                if l == 8:
                    if (i+1)%3 == 0:
                        string_ele= '\\N{}{}{}{}{'+a+'} \\\ \hline\hline'
                    else:
                        string_ele= '\\N{}{}{}{}{'+a+'} \\\ \hline'
                else:
                    if l ==8:
                        string_ele = '\\N{}{}{}{}{'+a+'} &'
                    else:
                        string_ele = '\\N{}{}{}{}{'+a+'} & '
                mid_str += string_ele
            if i ==8:
                mid_str += "\n"
            else:
                mid_str += "\n\n"
        
        number = filename[-5]
        if is_forced:
            name_file = "output/sudoku_"+str(number)+"_forced.tex"
        else:
            name_file = "output/sudoku_"+str(number)+"_bare.tex"
        f = open(name_file, "w")
        bot = self.bot_part
        f.write(self.top_part+mid_str+bot.rstrip()+"\n")
        f.close()
    
    def box_nums_1(self,grid,r,c):
        #takes 3 args grid, r ,c , 
        # returns LIST of nums 1-9 in the same box as r,c
        grid = grid
        return_list =[]
        di={0:0,1:3,2:6}
        r_i = di[r // 3]
        c_i = di[c // 3]
        for i in range(r_i,r_i+3):
            for k in range(c_i,c_i+3):
                if grid[i][k] > 0:
                    return_list.append(grid[i][k])
        return return_list
                    
        
    def find_marked(self):
        grid = self.sudoku_grid
        column_grid = list(zip(*grid))
        dic_for_marked = defaultdict(tuple)
        dic_with_occupied = defaultdict(tuple)
        full_set = set([i for i in range(1,10)])
        for i in range(9):
            for j in range(9):
                if grid[i][j] > 0:
                    dic_for_marked[(i,j)] = set() 
                else:
                    #when empty
                    #find all nums that could fit here
                    column_set = set(column_grid[j])
                    row_set = column_set.union(set(grid[i]))
                    box_set = row_set.union(set(self.box_nums_1(grid,i,j)))
                    box_set.remove(0)
#                     print("i",i,"j",j,"box",full_set - box_set)
                    #gives ordered set of numbers that can go in that cell
                    dic_for_marked[(i,j)] = full_set - box_set
                    dic_with_occupied[(i,j)] = box_set
#         print(dic_for_marked)
#         print(dic_with_occupied)
        return dic_with_occupied

    def get_box_cords(self,grid,r,c):
        grid = grid
        di={0:0,1:3,2:6}
        r_i = di[r // 3]
        c_i = di[c // 3]
        L =[]
        for i in range(r_i,r_i+3):
            for k in range(c_i,c_i+3):
                if grid[i][k] == 0:
                    if i!=r or k!=c:
                        L.append((i,k))
        return L
    
    def forced_tex_output(self):
        grid = self.sudoku_grid
        dic = self.find_marked()
        boolVal = True
        while boolVal:
            boolVal = False
            for i in range(9):
                for j in range(9):
                    if grid[i][j]==0:
                        list_of_coords = self.get_box_cords(grid,i,j) #list of tuples
    #                     print(i,j,list_of_coords)
                        intersected_set = set()
                        init_condition = True
                        for co_ord in list_of_coords:
                            local_set = dic[co_ord]
                            if init_condition:
                                intersected_set = local_set
                                init_condition = False
                            else:
                                intersected_set = intersected_set.intersection(local_set)
                        if len(intersected_set-set(self.box_nums_1(grid,i,j))) == 1:
                            boolVal = True
                            forced_num_set = (intersected_set-set(self.box_nums_1(grid,i,j)))
                            forced_num = list(forced_num_set)[0]
#                             print(i,j,"forced num: ",forced_num)
                            grid[i][j] = forced_num
                            self.sudoku_grid = grid
                            dic = self.find_marked()
        self.bare_tex_output(is_forced = True)
        '''================gets marked_tex ==============='''           
    def get_marked_set(self):
        grid = self.sudoku_grid
        column_grid = list(zip(*grid))
        dic_for_marked = defaultdict(tuple)
        dic_with_occupied = defaultdict(tuple)
        full_set = set([i for i in range(1,10)])
        for i in range(9):
            for j in range(9):
                if grid[i][j] > 0:
                    dic_for_marked[(i,j)] = set() 
                else:
                    #when empty
                    #find all nums that could fit here
                    column_set = set(column_grid[j])
                    row_set = column_set.union(set(grid[i]))
                    box_set = row_set.union(set(self.box_nums_1(grid,i,j)))
                    box_set.remove(0)
#                     print("i",i,"j",j,"box",full_set - box_set)
                    #gives ordered set of numbers that can go in that cell
                    dic_for_marked[(i,j)] = full_set - box_set
                    dic_with_occupied[(i,j)] = box_set
#         print(dic_for_marked)
#         print(dic_with_occupied)
        return dic_for_marked

    def format_to_print_markers(self, set_of_markers):
        top_l = ""
        top_r = ""
        bot_l = ""
        bot_r = ""
        
        if 1 in set_of_markers and 2 not in set_of_markers:
            top_l = "1"
        elif 2 in set_of_markers and 1 not in set_of_markers:
            top_l = "2"
        elif 1 in set_of_markers and 2 in set_of_markers:
            top_l = "1 2"
            
        if 3 in set_of_markers and 4 not in set_of_markers:
            top_r = "3"
        elif 4 in set_of_markers and 3 not in set_of_markers:
            top_r = "4"
        elif 3 in set_of_markers and 4 in set_of_markers:
            top_r = "3 4"
            
        if 5 in set_of_markers and 6 not in set_of_markers:
            bot_l = "5"
        elif 6 in set_of_markers and 5 not in set_of_markers:
            bot_l = "6"
        elif 5 in set_of_markers and 6 in set_of_markers:
            bot_l = "5 6"
        
        count = True 
        for i in sorted(set_of_markers):
            if i>= 7:
                if count:
                    bot_r += str(i)
                    count = False
                else:
                    bot_r += " "+str(i)
                
        
        
        return top_l,top_r, bot_l, bot_r
        
    def marked_tex_output(self):
        grid = self.sudoku_grid
        self.forced_tex_output()
        dic = self.get_marked_set()
        mid_str = ""
        for i in range(9):
            mid_str += f"% Line {i+1}\n"
            for j in range(0,3):
                cen_str = ""
                top_l = ""
                top_r = ""
                bot_l = ""
                bot_r = ""
                if grid[i][j]>0:
                    cen_str = str(grid[i][j])
                else:# center is 0, so has markers
                    set_of_markers = dic[(i,j)]
                    top_l,top_r,bot_l,bot_r = self.format_to_print_markers(set_of_markers)
#                     print(i,j,set_of_markers)
#                     pass 
                if j ==2:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} &'
                else:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} & '
#                 print(i,j,string_ele)
                mid_str += string_ele
            mid_str += "\n"
            for j in range(3,6):
                cen_str = ""
                top_l = ""
                top_r = ""
                bot_l = ""
                bot_r = ""
                if grid[i][j]>0:
                    cen_str = str(grid[i][j])
                else:# center is 0, so has markers
                    set_of_markers = dic[(i,j)]
                    top_l,top_r,bot_l,bot_r = self.format_to_print_markers(set_of_markers)
#                     print(i,j,set_of_markers)
#                     pass 
                if j ==5:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} &'
                else:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} & '
#                 print(i,j,string_ele)
                mid_str += string_ele
            mid_str += "\n"
            for j in range(6,9):
                cen_str = ""
                top_l = ""
                top_r = ""
                bot_l = ""
                bot_r = ""
                if grid[i][j]>0:
                    cen_str = str(grid[i][j])
                else:# center is 0, so has markers
                    set_of_markers = dic[(i,j)]
                    top_l,top_r,bot_l,bot_r = self.format_to_print_markers(set_of_markers)
#                     print(i,j,set_of_markers)
#                     pass 
                if j ==8:
                    if (i+1)%3 == 0:
                        string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} \\\ \hline\hline'
                    else:
                        string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} \\\ \hline'
                else:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} & '
#                 print(i,j,string_ele)
                mid_str += string_ele
            if i ==8:
                mid_str += "\n"
            else:
                mid_str += "\n\n"
#         print(mid_str)
        number = self.filename[-5]
        name_file = "output/sudoku_"+str(number)+"_marked.tex"
        f = open(name_file, "w")
        bot = self.bot_part
        f.write(self.top_part+mid_str+bot.rstrip()+"\n")
        f.close()
        
    #==== WORKED STARTS ===========================
    def get_points_in_same_grid(self,r,c):
        di={0:0,1:3,2:6}
        r_i = di[r // 3]
        c_i = di[c // 3]
        points_list = list()
        for i in range(r_i, r_i+3):
            for j in range(c_i,c_i+3):
                if i != r or j != c or True:
                    points_list.append((i,j))
        return points_list
    
    def get_points_in_same_row(self,r,c):
        L = list()
        for j in range(9):
            if j != c or True:
                L.append((r,j))
        return L
    
    def get_points_in_same_col(self,r,c):
        L = list()
        for i in range(9):
            if i != r or True:
                L.append((i,c))
        return L
    
    def initialize_cancelled_dict(self):
        dic_cancelled = defaultdict(tuple)
        for i in range(9):
            for j in range(9):
                dic_cancelled[(i,j)] = set()
        return dic_cancelled
                    
    def find_pre_emp_box(self,grid,r,c,dic_marked,marked_set):
        di={0:0,1:3,2:6}
        r_i = di[r // 3]
        c_i = di[c // 3]
        length = len(marked_set)
        count = 0
        pre_emp_list = list()
        pre_emp_list.append((r,c))
        for i in range(r_i, r_i+3):
            for j in range(c_i,c_i+3):
                if i != r or j != c:
                    local_set = dic_marked[(i,j)]
                    if local_set.issubset(marked_set) and len(local_set) != 0:
                        pre_emp_list.append((i,j))
                        count += 1
        if count == (length-1) and count>=1:
#             print("Count: ",count)
            return [True, pre_emp_list]
        else:
            return [False,[]]
        
    def find_pre_emp_row(self,grid,r,c,dic_marked,marked_set):
        count = 0 
        length = len(marked_set)
        pre_emp_list = list()
        pre_emp_list.append((r,c))
        box_points_list = self.get_points_in_same_grid(r,c)
        for j in range(9):
            if j != c:
#                 if (r,j) not in box_points_list:
                local_set = dic_marked[(r,j)]
                if local_set.issubset(marked_set) and len(local_set) != 0:
                        pre_emp_list.append((r,j))
                        count += 1
        some_bool = False
        for tup in pre_emp_list:
            if tup not in box_points_list:
                some_bool = True
                
        if count == (length-1) and count>=1 and some_bool:
            return [True, pre_emp_list]
        else:
            return [False,[]]
    
    def find_pre_emp_col(self,grid,r,c,dic_marked,marked_set):
        count = 0 
        length = len(marked_set)
        pre_emp_list = list()
        pre_emp_list.append((r,c))
        box_points_list = self.get_points_in_same_grid(r,c)
        for i in range(9):
            if i != r:
#                 if (i,c) not in box_points_list:
                local_set = dic_marked[(i,c)]
                if local_set.issubset(marked_set) and len(local_set) != 0:
                        pre_emp_list.append((i,c))
                        count += 1
        some_bool = False
        for tup in pre_emp_list:
            if tup not in box_points_list:
                some_bool = True
        if count == (length-1) and count>=1 and some_bool:
            return [True, pre_emp_list]
        else:
            return [False,[]]
    
    def singleton_aftermath(self,singleton_val,r,c, grid,dic_marked, dic_cancelled):
#         r = point[0]
#         c = point[1]
#         print(r,c,"=====Singleton=====", singleton_val)
        grid[r][c] = singleton_val
        dic_marked[(r,c)] -= set([singleton_val])
        placeholder = dic_cancelled[(r,c)]
        placeholder = placeholder.union(set([singleton_val]))
        dic_cancelled[(r,c)] = placeholder
        #         box
        box_list = self.get_points_in_same_grid(r,c)
        for point in box_list:
            if (singleton_val in dic_marked[point]) and (len(dic_marked[point])>1):
#                 print("here=====", singleton_val)
#                 grid[point[0]][point[1]] = singleton_val
                dic_marked[point] -= set([singleton_val])
                placeholder = dic_cancelled[point]
                placeholder = placeholder.union(set([singleton_val]))
                dic_cancelled[point] = placeholder
                if len(dic_marked[point])==1:
                    
                    new_s_set = dic_marked[point]
                    new_s_val = new_s_set.pop()
#                     grid[point[0]][point[1]] = new_s_val
                    grid,dic_marked,dic_cancelled = self.singleton_aftermath(new_s_val,point[0],point[1],grid,dic_marked,dic_cancelled)

        #row
        for j in range(9):
            if j != c:
                if singleton_val in dic_marked[(r,j)] and len(dic_marked[(r,j)])>1:
#                     grid[r][j] = singleton_val
                    dic_marked[(r,j)] -= set([singleton_val])
                    placeholder = dic_cancelled[(r,j)]
                    placeholder = placeholder.union(set([singleton_val]))
                    dic_cancelled[(r,j)] = placeholder
                    if len(dic_marked[(r,j)]) ==1:
                        new_s_set = dic_marked[(r,j)]
                        new_s_val = new_s_set.pop()
#                         grid[r][j] = new_s_val
                        grid,dic_marked,dic_cancelled = self.singleton_aftermath(new_s_val,r,j,grid, dic_marked,dic_cancelled)
        
#         col
        for i in range(9):
            if i!=r:
                if singleton_val in dic_marked[(i,c)] and len(dic_marked[(i,c)])>1:
#                     grid[i][c] = singleton_val
                    dic_marked[(i,c)] -= set([singleton_val])
                    placeholder = dic_cancelled[(i,c)]
                    placeholder = placeholder.union(set([singleton_val]))
                    dic_cancelled[(i,c)] = placeholder
                    
                    if len(dic_marked[(i,c)]) == 1:
                        new_s_set = dic_marked[(i,c)]
                        new_s_val = new_s_set.pop()
#                         grid[i][c] = new_s_val
                        grid,dic_marked,dic_cancelled = self.singleton_aftermath(new_s_val,i,c,grid, dic_marked,dic_cancelled)
                    
        return grid,dic_marked, dic_cancelled
    
    def find_forced_singleton(self, r,c,grid, dic_marked, dic_cancelled):
        
        row_points = self.get_points_in_same_row(r,c)#same row ALL points
        row_points.remove((r,c))
        col_points = self.get_points_in_same_col(r,c)
        col_points.remove((r,c))
        box_points = self.get_points_in_same_grid(r,c)
        box_points.remove((r,c))
        
        marked_set = dic_marked[(r,c)]
        
        intersected_set_3 = marked_set
        for point in box_points:
            intersected_set_3 = intersected_set_3 - dic_marked[point]
        
        intersected_set_1 = marked_set
        for point in col_points:
            intersected_set_1 = intersected_set_1 - dic_marked[point]
        
        intersected_set_2 = marked_set
        for point in row_points:
            intersected_set_2 = intersected_set_2 - dic_marked[point]
        
        main_intersected = set()
        if len(intersected_set_3)==1:
            main_intersected = intersected_set_3
        elif len(intersected_set_2)==1:
            main_intersected = intersected_set_2
        elif len(intersected_set_1)==1:
            main_intersected = intersected_set_1
        else:
            main_intersected = set()
            
        if len(main_intersected) ==1:
            for_val_set = main_intersected
            place_holder = dic_cancelled[(r,c)]
            place_holder_marked = dic_marked[(r,c)]
            place_holder = place_holder.union(place_holder_marked)
            dic_cancelled[(r,c)] = place_holder
            dic_marked[(r,c)] = set()
            grid,dic_marked,dic_cancelled = self.singleton_aftermath(for_val_set.pop(),r,c,grid,dic_marked,dic_cancelled)
        return grid, dic_marked, dic_cancelled
    
    def get_formatted(self,i,j,grid,dic_marked,dic_cancelled):
        aa = dic_marked[(i,j)]
        bb = dic_cancelled[(i,j)]
        set_of_markers = aa.union(bb)
#         print(aa,bb,set_of_markers)
        top_l = ""
        top_r = ""
        bot_l = ""
        bot_r = ""
        new1 = True
        new2 = True
        new3 = True
        new4 = True 
        for i in sorted(set_of_markers):
            if i>=1 and i<=2:
                if new1:
                    new1 = False
                    if i in aa:
                        top_l += str(i)
                    elif i in bb:
                        top_l += "\\cancel{"+str(i)+"}"
                else:
                    if i in aa:
                        top_l += " "+str(i)
                    elif i in bb:
                        top_l += " "+"\\cancel{"+str(i)+"}"
            elif i>=3 and i<=4:
                if new2:
                    new2 = False
                    if i in aa:
                        top_r += str(i)
                    elif i in bb:
                        top_r += "\\cancel{"+str(i)+"}"
                else:
                    if i in aa:
                        top_r += " "+str(i)
                    elif i in bb:
                        top_r += " "+"\\cancel{"+str(i)+"}"
            elif i>=5 and i<=6:
                if new3:
                    new3 = False
                    if i in aa:
                        bot_l += str(i)
                    elif i in bb:
                        bot_l += "\\cancel{"+str(i)+"}"
                else:
                    if i in aa:
                        bot_l += " "+str(i)
                    elif i in bb:
                        bot_l += " "+"\\cancel{"+str(i)+"}"
            elif i>= 7:
                if new4:
                    new4 = False
                    if i in aa:
                        bot_r += str(i)
                    elif i in bb:
                        bot_r += "\\cancel{"+str(i)+"}"
                else:
                    if i in aa:
                        bot_r += " "+str(i)
                    elif i in bb:
                        bot_r += " "+"\\cancel{"+str(i)+"}"
        return top_l,top_r,bot_l,bot_r
                    
        
    def print_final(self,grid, dic_marked,dic_cancelled):
        
        mid_str = ""
        for i in range(9):
            mid_str += f"% Line {i+1}\n"
            for j in range(0,3):
                cen_str = ""
                top_l = ""
                top_r = ""
                bot_l = ""
                bot_r = ""
                if grid[i][j]>0:
                    cen_str = str(grid[i][j])
                    top_l,top_r,bot_l,bot_r = self.get_formatted(i,j,grid,dic_marked,dic_cancelled)
                else:
                    cen_str = ""
                    top_l,top_r,bot_l,bot_r = self.get_formatted(i,j,grid,dic_marked,dic_cancelled)
                if j ==2:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} &'
                else:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} & '
                mid_str += string_ele
            mid_str += "\n"
            for j in range(3,6):
                cen_str = ""
                top_l = ""
                top_r = ""
                bot_l = ""
                bot_r = ""
                if grid[i][j]>0:
                    cen_str = str(grid[i][j])
                    top_l,top_r,bot_l,bot_r = self.get_formatted(i,j,grid,dic_marked,dic_cancelled)
                else:
                    cen_str = ""
                    top_l,top_r,bot_l,bot_r = self.get_formatted(i,j,grid,dic_marked,dic_cancelled)
                if j ==5:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} &'
                else:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} & '
                mid_str += string_ele
            mid_str += "\n"
            for j in range(6,9):
                cen_str = ""
                top_l = ""
                top_r = ""
                bot_l = ""
                bot_r = ""
                if grid[i][j]>0:
                    cen_str = str(grid[i][j])
                    top_l,top_r,bot_l,bot_r = self.get_formatted(i,j,grid,dic_marked,dic_cancelled)
                else:
                    cen_str = ""
                    top_l,top_r,bot_l,bot_r = self.get_formatted(i,j,grid,dic_marked,dic_cancelled)
                if j ==8:
                    if (i+1)%3 == 0:
                        string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} \\\ \hline\hline'
                    else:
                        string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} \\\ \hline'
                else:
                    string_ele = '\\N{'+top_l+'}{'+top_r+'}{'+bot_l+'}{'+bot_r+'}{'+cen_str+'} & '
                    
                mid_str += string_ele
            if i ==8:
                mid_str += "\n"
            else:
                mid_str += "\n\n"
            
        number = self.filename[-5]
        name_file = "output/sudoku_"+str(number)+"_worked.tex"
        f = open(name_file, "w")
        bot = self.bot_part
        f.write(self.top_part+mid_str+bot.rstrip()+"\n")
        f.close()
        
    def worked_tex_output(self):
        '''
        local vars are grid, dic_cancelled, dic_marked
        '''
        self.marked_tex_output()
        
        grid = self.sudoku_grid
        dic_cancelled = self.initialize_cancelled_dict()
        dic_marked = self.get_marked_set()
        find_again = True
        cc = 0
        overall_pre_emps = list()
        #===========ADD WHILE LOOP========= while find_again:
        while find_again:
            cc +=1
            find_again = False
            for i in range(9):
                for j in range(9):
                    #TESTING
                    if grid[i][j]==0:
                        grid, dic_marked, dic_cancelled = self.find_forced_singleton(i,j,grid,dic_marked,dic_cancelled)

                        #BOX
                        marked_set = dic_marked[(i,j)]
                        length = len(marked_set)
                        if  length >= 2:
                            returned_val_box = self.find_pre_emp_box(grid,i,j,dic_marked, marked_set)
                            has_pre_emp_box = returned_val_box[0]
                            pre_emp_cords_list_box = returned_val_box[1]

                            if has_pre_emp_box:
    #                             print("preemp found box",i,j,pre_emp_cords_list_box)
                                if pre_emp_cords_list_box not in overall_pre_emps:
                                    overall_pre_emps.append(pre_emp_cords_list_box)
                                    find_again = True
                                else:
                                    find_again = False
                                same_grid_points_list = self.get_points_in_same_grid(i,j)

                                for point in same_grid_points_list:
                                    if point not in pre_emp_cords_list_box:
                                        points_marked_set = dic_marked[point]
                                        if len(points_marked_set) >= 2:
                                            new_marked_set = points_marked_set-marked_set
                                            dic_marked[point] = new_marked_set
                                            add_cancelled_set = points_marked_set - new_marked_set
        #                                     print("add_cancelled_set", add_cancelled_set)
                                            cancelled_placeholder = dic_cancelled[point] 
                                            dic_cancelled[point] = cancelled_placeholder.union(add_cancelled_set)

                                            if len(new_marked_set)==1:
                                                val =new_marked_set.pop()
                                                grid,dic_marked,dic_cancelled = self.singleton_aftermath(val,point[0],point[1],grid,dic_marked,dic_cancelled)

                                        elif len(points_marked_set)==1:
                                            grid,dic_marked,dic_cancelled = self.singleton_aftermath(val,point[0],point[1],grid,dic_marked,dic_cancelled)
                        #ROW
                        marked_set = dic_marked[(i,j)]
                        length = len(marked_set)
                        if length>=2:
                            returned_val_row = self.find_pre_emp_row(grid,i,j,dic_marked, marked_set)
                            has_pre_emp_row = returned_val_row[0]
                            pre_emp_cords_list_row = returned_val_row[1]
                            if has_pre_emp_row:
        #                         print("here123",i,j, pre_emp_cords_list_row)
                                if pre_emp_cords_list_row not in overall_pre_emps:
                                    overall_pre_emps.append(pre_emp_cords_list_row)
                                    find_again = True
                                else:
                                    find_again = False 
                                same_grid_points_list = self.get_points_in_same_row(i,j)

                                for point in same_grid_points_list:
                                    if point not in pre_emp_cords_list_row:
                                        points_marked_set = dic_marked[point]
                                        if len(points_marked_set) >= 2:
                                            new_marked_set = points_marked_set-marked_set
                                            dic_marked[point] = new_marked_set
                                            add_cancelled_set = points_marked_set - new_marked_set
        #                                     print("add_cancelled_set", add_cancelled_set)
                                            cancelled_placeholder = dic_cancelled[point] 
                                            dic_cancelled[point] = cancelled_placeholder.union(add_cancelled_set)     
                                            if len(new_marked_set)==1:
                                                val =new_marked_set.pop()
                                                grid,dic_marked,dic_cancelled = self.singleton_aftermath(val,point[0],point[1],grid,dic_marked,dic_cancelled)

                                        elif len(points_marked_set)==1:
                                            grid,dic_marked,dic_cancelled = self.singleton_aftermath(val,point[0],point[1],grid,dic_marked,dic_cancelled)


                        #==========================================
                        marked_set = dic_marked[(i,j)]
                        length = len(marked_set)
                        if length>=2:
                            returned_val_col = self.find_pre_emp_col(grid,i,j,dic_marked, marked_set)
                            has_pre_emp_col = returned_val_col[0]
                            pre_emp_cords_list_col = returned_val_col[1]

                            if has_pre_emp_col:
        #                         print("here123",i,j, pre_emp_cords_list_row)
                                if pre_emp_cords_list_col not in overall_pre_emps:
                                    overall_pre_emps.append(pre_emp_cords_list_col)
                                    find_again = True
                                else:
                                    find_again = False 
                                same_grid_points_list = self.get_points_in_same_col(i,j)

                                for point in same_grid_points_list:
                                    if point not in pre_emp_cords_list_col:
                                        points_marked_set = dic_marked[point]
                                        if len(points_marked_set) >= 2:
                                            new_marked_set = points_marked_set-marked_set
                                            dic_marked[point] = new_marked_set
                                            add_cancelled_set = points_marked_set - new_marked_set
        #                                     print("add_cancelled_set", add_cancelled_set)
                                            cancelled_placeholder = dic_cancelled[point] 
                                            dic_cancelled[point] = cancelled_placeholder.union(add_cancelled_set)

                                            if len(new_marked_set)==1:
                                                val =new_marked_set.pop()
                                                grid,dic_marked,dic_cancelled = self.singleton_aftermath(val,point[0],point[1],grid,dic_marked,dic_cancelled)

                                        elif len(points_marked_set)==1:
                                            grid,dic_marked,dic_cancelled = self.singleton_aftermath(val,point[0],point[1],grid,dic_marked,dic_cancelled)


                        if len(dic_marked[(i,j)])>=1:
                            has_hidden_singleton = False
                    
          
#         for line in grid:
#             print(line)
#         for i in range(0,9):
#             for j in range(0,9):
#                 if len(dic_cancelled[(i,j)])>0:
#                     print(i,j,"marked: ",dic_marked[(i,j)],"cancelled: ", dic_cancelled[(i,j)])
#                     pass 
        self.print_final(grid,dic_marked,dic_cancelled)
                                
                            
                        
                        
                        
                        
                        
                    
                
        
            
        