from sudoku import *

for i in range(1,8):
	if i !=6 :
		file_location = 'assets/test_docs/sudoku_'+str(i)+'.txt'
		print("===============================")
		print("Assessing sudoku_"+str(i)+'.txt')
		print("===============================")
		sudoku = Sudoku(file_location)
		is_accessible = sudoku.preassess()
		print(is_accessible+ "\n")
		if is_accessible == "There might be a solution.":
			sudoku.bare_tex_output()
			print("sudoku_"+str(i)+"_bare.tex output file is created.\nThis latex file views the sudoku grid on a latex file\n")

			sudoku.forced_tex_output()
			print("sudoku_"+str(i)+"_forced.tex output file is created.\nThis latex file adds the forced digit solutions to the bare latex file\n")
			
			sudoku.marked_tex_output()
			print("sudoku_"+str(i)+"_marked.tex output file is created.\nThis latex file adds all possible digits in a cell to the corners of the empty cells.\n")
			sudoku.worked_tex_output()
			print("sudoku_"+str(i)+"_worked.tex output file is created.\nThis latex file depicts the grid where all forced digits have been added,\n\tall possible digits have been added to the corners of the empty cells,\n\tand the preemptive set technique has been applied until it cannot allow one to eliminate any possible digit from any cell\n")

		